/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_select.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/10 17:23:25 by rvievill          #+#    #+#             */
/*   Updated: 2016/09/17 12:13:59 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SELECT_H
# define FT_SELECT_H

# include <unistd.h>
# include <term.h>
# include <sys/ioctl.h>
# include <fcntl.h>
# include <stdlib.h>
# include <signal.h>
# include <termios.h>
# include <curses.h>
# include <termcap.h>
# include "../libft/libft.h"

typedef struct		s_arg
{
	int				start;
	char			*name;
	int				selected;
	int				current;
	struct s_arg	*next;
	struct s_arg	*prev;
}					t_arg;

typedef struct		s_info
{
	t_arg			*arg;
	struct termios	term;
	int				fd;
	int				enter;
	int				line;
	int				column;
	int				len;
	int				nb_param;
}					t_info;

enum				e_key_code
{
	LEFT_A = 4479771,
	RIGHT_A = 4414235,
	UP_A = 4283163,
	DOWN_A = 4348699,
	CTRL_A = 1,
	ENTER = 10,
	DELETE = 2117294875,
	ESC = 27,
	BACKSPACE = 127,
	SPACE = 32,
	CTRL_D = 4
}					;

/*
** style_print.c
*/
void				style_print(t_arg *arg, int fd);
/*
** utils.c
*/
void				screen_clear(void);
int					my_putchar(int c);
t_info				*recovery_struct(t_info *info, int i);
/*
** create_lst.c
*/
void				create_lst(int ac, char **av, t_info *info);
void				fake_node(t_arg *arg, int nb, t_arg *first);
void				fill_info_size(t_info *info, int ac);
/*
** main.c
*/
int					put_error(int er);
/*
** init_term.c
*/
int					init_term(int ac, t_info *info, char **av);
void				init_termios(struct termios *info);
int					term_default(t_info *info);
/*
** print_lst.c
*/
void				print_lst(t_info info);
/*
** move.c
*/
void				left(t_info *info);
void				right(t_info *info);
void				up(t_info *info);
void				down(t_info *info);
/*
** action.c
*/
void				selecte(t_info *info);
int					delet(t_info *info);
void				free_fake_node(t_info *info);
/*
** move_spe.c
*/
void				select_all(t_info *info);
void				deselect_all(t_info *info);
void				enter(t_info *info);
/*
** key_select.c
*/
int					action_select(char **buff, t_info *info);
/*
** signal.c
*/
void				catch_signal(void);
void				ctrl_z(int sig);
/*
** catch_key.c
*/
int					catch_key(t_info *info);
/*
** check_size.c
*/
int					check_size(t_info *info);
/*
** free_lst.c
*/
void				free_lst(t_arg *cur);
void				free_node(t_arg *node);

#endif
