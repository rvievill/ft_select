# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: rvievill <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/07/14 11:57:02 by rvievill          #+#    #+#              #
#    Updated: 2016/09/12 18:49:42 by rvievill         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = ft_select

SRC_NAMES =	action.c \
			action_spe.c \
			catch_key.c \
			check_size.c \
			create_lst.c \
			key_select.c \
			main.c \
			move.c \
			print_lst.c \
			set_term.c \
			signal.c \
			style_print.c \
			free_lst.c \
			utils.c

OBJ_NAMES = $(SRC_NAMES:.c=.o)
INC_NAMES = libft.h
LIB_NAMES = libft.a

SRC_PATH = ./src
OBJ_PATH = ./obj
INC_PATH = ./include ./libft
LIB_PATH = ./libft/

SRC = $(addprefix $(SRC_PATH)/,$(SRC_NAMES))
OBJ = $(addprefix $(OBJ_PATH)/,$(OBJ_NAMES))
INC = $(addprefix -I,$(INC_PATH))
LIB = $(LIB_PATH)$(LIB_NAMES)

################################################################################

FLAGS = -Wall -Wextra -Werror
LDLIBS = -lft
COM = gcc

################################################################################

all: $(LIB) $(NAME)

$(NAME): $(LIB) $(OBJ)
	@make -C libft
	$(COM) -ltermcap $^ -o $@ -L $(LIB_PATH) -lft

$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	@mkdir $(OBJ_PATH) 2> /dev/null || true
	$(COM) $(FLAGS) $(INC) -o $@ -c $<

$(LIB):
	@make -C $(LIB_PATH)

clean:
	@make clean -C libft
	@rm -rf $(OBJ_PATH) 2> /dev/null || true
	@rmdir $(OBJ_PATH) obj 2> /dev/null || echo  > /dev/null
	@rm -f $(OBJ)

fclean: clean
	@make fclean -C libft
	@rm -rf $(NAME)

re: clean fclean all

.PHONY: all clean fclean re
