/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdupchr.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/15 14:55:15 by rvievill          #+#    #+#             */
/*   Updated: 2016/07/17 18:05:05 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strdupchr(char *s1, char c)
{
	char	*res;
	int		i;

	i = 0;
	if (s1 == NULL)
		return (NULL);
	if ((res = ft_memalloc(ft_strlen(s1))) == NULL)
		return (NULL);
	while (s1[i] && s1[i] != c)
	{
		res[i] = s1[i];
		i++;
	}
	res[i] = '\0';
	return (res);
}
