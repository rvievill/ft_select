/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strusplit.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/14 11:53:35 by rvievill          #+#    #+#             */
/*   Updated: 2016/09/06 19:42:08 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strusplit(char **str, char *c)
{
	char	*line;

	line = ft_strdup("");
	while (*str)
	{
		line = ft_strjoin(line, *str);
		line = ft_strjoin(line, c);
		str++;
	}
	return (line);
}
