/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   signal.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/05 16:42:29 by rvievill          #+#    #+#             */
/*   Updated: 2016/09/13 13:10:31 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_select.h"

static void				ctrl_z_re(void)
{
	t_info				*tmp;
	struct termios		cur;
	char				*name;

	tmp = NULL;
	tmp = recovery_struct(tmp, 0);
	tcgetattr(0, &cur);
	init_termios(&cur);
	tputs(tgetstr("vi", 0), 1, my_putchar);
	name = ttyname(1);
	if ((tmp->fd = open(name, O_WRONLY)) == -1)
		ft_putstr_fd("open failed\n", 2);
	tcsetattr(0, TCSADRAIN, &cur);
	screen_clear();
	print_lst(*tmp);
	signal(SIGTSTP, ctrl_z);
}

static void				resize(void)
{
	t_info				*tmp;
	struct winsize		size;
	t_arg				*start;

	tmp = NULL;
	tmp = recovery_struct(tmp, 0);
	start = tmp->arg;
	free_fake_node(tmp);
	ioctl(0, TIOCGWINSZ, &size);
	tmp->column = size.ws_col / tmp->len;
	if ((tmp->line * tmp->column) == tmp->nb_param)
		tmp->line = tmp->nb_param / tmp->column;
	else
		tmp->line = tmp->nb_param / tmp->column + 1;
	tmp->nb_param < tmp->column ? tmp->column = tmp->nb_param : 0;
	tmp->arg = tmp->arg->prev;
	fake_node(tmp->arg, (tmp->column * tmp->line) - tmp->nb_param, start);
	tmp->arg = start;
	screen_clear();
	check_size(tmp);
}

void					ctrl_z(int sig)
{
	t_info				*tmp;
	char				p;

	(void)sig;
	tmp = NULL;
	tmp = recovery_struct(tmp, 0);
	p = tmp->term.c_cc[VSUSP];
	if (ioctl(0, TIOCSTI, &p) == -1)
		ft_putendl_fd("ioctl fail", 2);
	else
		signal(sig, SIG_DFL);
	term_default(tmp);
	tcsetattr(0, 0, &tmp->term);
	screen_clear();
}

static void				identify_sig(int sig)
{
	if (sig == SIGTSTP)
		ctrl_z(sig);
	else if (sig == SIGCONT)
		ctrl_z_re();
	else if (sig == SIGWINCH)
		resize();
	else
		signal(sig, SIG_IGN);
}

void					catch_signal(void)
{
	int					i;

	i = 1;
	while (i < 32)
	{
		signal(i, identify_sig);
		i++;
	}
}
