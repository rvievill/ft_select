/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   style_print.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/23 14:10:35 by rvievill          #+#    #+#             */
/*   Updated: 2016/09/17 13:28:56 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_select.h"
#include <sys/stat.h>

static void			color_arg(char *arg, int fd)
{
	struct stat		info;

	if (lstat(arg, &info) != -1)
	{
		if (S_ISDIR(info.st_mode))
			ft_putstr_fd("\033[34m", fd);
		else if (S_ISLNK(info.st_mode))
			ft_putstr_fd("\033[35m", fd);
		else if (S_ISCHR(info.st_mode) || S_ISBLK(info.st_mode))
			ft_putstr_fd("\033[33m", fd);
		else if (S_ISSOCK(info.st_mode))
			ft_putstr_fd("\033[32m", fd);
		else if (info.st_mode & S_IXUSR || info.st_mode & S_IXGRP
		|| info.st_mode & S_IXOTH)
			ft_putstr_fd("\033[31m", fd);
	}
	ft_putstr_fd(arg, fd);
	ft_putstr_fd("\033[0m", fd);
}

static void			underline(char *arg, int fd)
{
	ft_putstr_fd("\033[4;37m", fd);
	color_arg(arg, fd);
	ft_putstr_fd("\033[0m", fd);
}

static void			highlight(char *arg, int fd)
{
	ft_putstr_fd("\033[7;37m", fd);
	color_arg(arg, fd);
	ft_putstr_fd("\033[0m", fd);
}

static void			underline_highlight(char *arg, int fd)
{
	ft_putstr_fd("\033[7;4;37m", fd);
	color_arg(arg, fd);
	ft_putstr_fd("\033[0m", fd);
}

void				style_print(t_arg *arg, int fd)
{
	if (arg->name)
	{
		if (arg->selected == 1 && arg->current == 0)
			highlight(arg->name, fd);
		else if (arg->selected == 0 && arg->current == 1)
			underline(arg->name, fd);
		else if (arg->selected == 1 && arg->current == 1)
			underline_highlight(arg->name, fd);
		else
			color_arg(arg->name, fd);
	}
}
