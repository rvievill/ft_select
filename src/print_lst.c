/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_lst.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/18 16:48:49 by rvievill          #+#    #+#             */
/*   Updated: 2016/09/17 13:28:42 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_select.h"

static void		alligne(int len, int size, int fd)
{
	int			i;

	i = 0;
	while ((i + size) < len)
	{
		ft_putchar_fd(' ', fd);
		i++;
	}
}

void			print_lst(t_info info)
{
	int			line;
	int			column;
	t_arg		*tmp;

	line = 0;
	tmp = info.arg;
	while (line < info.line)
	{
		column = 0;
		style_print(tmp, info.fd);
		alligne(info.len, (int)ft_strlen(tmp->name), info.fd);
		tmp = tmp->next;
		while (tmp && tmp->start != 1 && column < info.column - 1)
		{
			style_print(tmp, info.fd);
			alligne(info.len, (int)ft_strlen(tmp->name), info.fd);
			column++;
			tmp = tmp->next;
		}
		ft_putchar_fd('\n', info.fd);
		line++;
	}
}
