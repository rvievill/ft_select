/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_lst.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/08 18:12:56 by rvievill          #+#    #+#             */
/*   Updated: 2016/09/08 18:49:34 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_select.h"

void		free_node(t_arg *node)
{
	if (node && node->name)
	{
		free(node->name);
		node->name = NULL;
	}
	if (node)
	{
		free(node);
		node = NULL;
	}
}

void		free_lst(t_arg *cur)
{
	t_arg	*begin;

	if (!cur->name)
		return ;
	begin = cur;
	cur = cur->next;
	while (cur && cur != begin)
	{
		free_node(cur);
		cur = cur->next;
	}
	free_node(cur);
	cur = NULL;
}
