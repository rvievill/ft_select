/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_size.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/06 13:19:36 by rvievill          #+#    #+#             */
/*   Updated: 2016/09/17 13:29:06 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_select.h"

int					check_size(t_info *info)
{
	struct winsize	size;
	static int		i = 0;

	ioctl(0, TIOCGWINSZ, &size);
	if (size.ws_row - 1 < info->line || size.ws_col - 1 < info->column)
	{
		screen_clear();
		i = 1;
		ft_putstr("window is too small !\n");
		return (0);
	}
	else
	{
		if (i == 1)
		{
			screen_clear();
			i = 0;
		}
		print_lst(*info);
	}
	return (1);
}
