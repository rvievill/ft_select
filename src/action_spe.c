/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   action_spe.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/24 17:44:37 by rvievill          #+#    #+#             */
/*   Updated: 2016/09/08 19:21:33 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_select.h"

void			select_all(t_info *info)
{
	t_arg		*tmp;

	tmp = info->arg;
	if (tmp->selected == 0)
		tmp->selected = 1;
	tmp = tmp->next;
	while (tmp->start != 1)
	{
		if (tmp->name != NULL && tmp->selected == 0)
			tmp->selected = 1;
		tmp = tmp->next;
	}
}

void			deselect_all(t_info *info)
{
	t_arg		*tmp;

	tmp = info->arg;
	if (tmp->selected == 1)
		tmp->selected = 0;
	tmp = tmp->next;
	while (tmp->start != 1)
	{
		if (tmp->selected == 1)
			tmp->selected = 0;
		tmp = tmp->next;
	}
}
