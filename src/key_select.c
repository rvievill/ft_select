/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_select.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/02 15:24:22 by rvievill          #+#    #+#             */
/*   Updated: 2016/09/12 11:53:47 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_select.h"

int				action_select(char **buff, t_info *info)
{
	if (*(int *)buff == CTRL_A)
		select_all(info);
	else if (*(int *)buff == BACKSPACE || *(int *)buff == DELETE)
		return (delet(info));
	else if (*(int *)buff == CTRL_D)
		deselect_all(info);
	else if (*(int *)buff == SPACE)
		selecte(info);
	else if (*(int *)buff == UP_A)
		up(info);
	else if (*(int *)buff == DOWN_A)
		down(info);
	else if (*(int *)buff == LEFT_A)
		left(info);
	else if (*(int *)buff == RIGHT_A)
		right(info);
	return (0);
}
