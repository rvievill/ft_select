/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/23 15:27:56 by rvievill          #+#    #+#             */
/*   Updated: 2016/09/12 15:58:02 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_select.h"

void			left(t_info *info)
{
	t_arg		*tmp;

	tmp = info->arg;
	while (tmp->current != 1)
		tmp = tmp->prev;
	tmp->current = 0;
	while (tmp->prev->name == NULL)
		tmp = tmp->prev;
	tmp->prev->current = 1;
}

void			right(t_info *info)
{
	t_arg		*tmp;

	tmp = info->arg;
	while (tmp->current != 1)
		tmp = tmp->next;
	tmp->current = 0;
	while (tmp->next->name == NULL)
		tmp = tmp->next;
	tmp->next->current = 1;
}

void			up(t_info *info)
{
	t_arg		*tmp;
	int			i;

	i = 0;
	tmp = info->arg;
	while (tmp->current != 1)
		tmp = tmp->prev;
	tmp->current = 0;
	while (i < info->column)
	{
		tmp = tmp->prev;
		i++;
	}
	i = 0;
	if (tmp->name == NULL)
	{
		while (i < info->column)
		{
			tmp = tmp->prev;
			i++;
		}
	}
	tmp->current = 1;
}

void			down(t_info *info)
{
	t_arg		*tmp;
	int			i;

	i = 0;
	tmp = info->arg;
	while (tmp->current != 1)
		tmp = tmp->next;
	tmp->current = 0;
	while (i < info->column)
	{
		tmp = tmp->next;
		i++;
	}
	i = 0;
	if (tmp->name == NULL)
	{
		while (i < info->column)
		{
			tmp = tmp->next;
			i++;
		}
	}
	tmp->current = 1;
}
