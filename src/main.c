/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/10 16:23:05 by rvievill          #+#    #+#             */
/*   Updated: 2016/09/17 13:28:18 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_select.h"

int				put_error(int er)
{
	if (er == 1)
		ft_putstr_fd("missing arguments\n", 2);
	if (er == 3)
		ft_putstr_fd("terminfo database could not  be  found\n", 2);
	if (er == 4)
		ft_putstr_fd("termios failed filling\n", 2);
	if (er == 5)
		ft_putstr_fd("unappled change\n", 2);
	if (er == -1)
		ft_putstr_fd("term not found\n", 2);
	return (er);
}

int				main(int ac, char **av)
{
	t_info		info;

	if (ac < 2)
		return (put_error(1));
	if (init_term(ac, &info, av) != 0)
		return (1);
	catch_signal();
	recovery_struct(&info, 1);
	while (1)
	{
		tputs(tgoto(tgetstr("cm", 0), 0, 0), 1, my_putchar);
		check_size(&info);
		if (catch_key(&info) == 1)
			break ;
	}
	term_default(&info);
	return (0);
}
