/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   catch_key.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/06 13:04:27 by rvievill          #+#    #+#             */
/*   Updated: 2016/09/14 18:17:13 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_select.h"

int			catch_key(t_info *info)
{
	char	*buff[5];

	*(int *)buff = 0;
	read(0, buff, 4);
	if (action_select(buff, info) == 1)
		return (1);
	if (*(int *)buff == ENTER)
	{
		info->enter = 1;
		return (1);
	}
	if (*(int *)buff == ESC)
		return (1);
	return (0);
}
