/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_lst.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/11 14:05:47 by rvievill          #+#    #+#             */
/*   Updated: 2016/09/17 12:11:55 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_select.h"

static void			init_node(t_arg *arg, char *av)
{
	arg->start = 1;
	arg->name = ft_strdup(av);
	arg->selected = 0;
	arg->current = 1;
	arg->prev = arg;
	arg->next = arg;
}

static void			new_node(char *av, t_arg *arg, t_arg *first_node)
{
	t_arg			*new;

	if ((new = (t_arg *)malloc(sizeof(t_arg))))
	{
		new->start = 0;
		new->name = ft_strdup(av);
		new->selected = 0;
		new->current = 0;
		new->prev = arg;
		new->next = first_node;
		arg->next = new;
	}
}

void				fake_node(t_arg *arg, int nb, t_arg *first)
{
	int				i;
	t_arg			*new;

	i = 0;
	while (i < nb)
	{
		if ((new = (t_arg *)malloc(sizeof(t_arg))))
		{
			new->name = NULL;
			new->selected = 0;
			new->current = 0;
			new->prev = arg;
			new->next = first;
			arg->next = new;
		}
		i++;
		arg = arg->next;
		first->prev = new;
	}
}

void				fill_info_size(t_info *info, int ac)
{
	t_arg			*tmp;
	struct winsize	size;

	info->len = 0;
	tmp = info->arg;
	info->len = ft_strlen(tmp->name);
	tmp = tmp->next;
	ioctl(0, TIOCGWINSZ, &size);
	while (tmp->name && tmp != info->arg)
	{
		if ((int)ft_strlen(tmp->name) > info->len)
			info->len = ft_strlen(tmp->name);
		tmp = tmp->next;
	}
	info->len++;
	info->nb_param = ac - 1;
	info->column = size.ws_col / info->len;
	info->line = info->nb_param / info->column;
	if ((info->line * info->column) == info->nb_param)
		info->line = (info->nb_param / info->column);
	else
		info->line = (info->nb_param / info->column) + 1;
	info->nb_param < info->column ? info->column = info->nb_param : 0;
}

void				create_lst(int ac, char **av, t_info *info)
{
	t_arg			*first;
	int				i;

	i = 0;
	if (!(info->arg = (t_arg *)malloc(sizeof(t_arg))))
		return ;
	info->enter = 0;
	init_node(info->arg, av[i++]);
	first = info->arg;
	while (av[i])
	{
		new_node(av[i], info->arg, first);
		info->arg = info->arg->next;
		i++;
	}
	info->arg->next->prev = info->arg;
	fill_info_size(info, ac);
	fake_node(info->arg, (info->column * info->line) - info->nb_param, first);
	info->arg = first;
}
