/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/10 16:08:13 by rvievill          #+#    #+#             */
/*   Updated: 2016/09/10 16:56:09 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_select.h"

t_info			*recovery_struct(t_info *info, int i)
{
	static t_info	*save = NULL;

	if (i == 1)
		save = info;
	return (save);
}

int				my_putchar(int c)
{
	return (write(2, &c, 1));
}

void			screen_clear(void)
{
	tputs(tgetstr("cl", 0), 1, my_putchar);
}
