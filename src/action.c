/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   action.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/23 15:56:32 by rvievill          #+#    #+#             */
/*   Updated: 2016/09/13 12:12:08 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_select.h"

static int			nb_selected(t_arg *arg)
{
	t_arg			*tmp;
	int				i;

	tmp = arg;
	i = 0;
	if (tmp->selected == 1)
		i++;
	tmp = tmp->next;
	while (tmp != arg)
	{
		if (tmp->selected == 1)
			i++;
		tmp = tmp->next;
	}
	return (i);
}

void				enter(t_info *info)
{
	t_arg			*tmp;
	int				nb;

	nb = nb_selected(info->arg);
	tmp = info->arg;
	screen_clear();
	if (info->arg->selected == 1)
	{
		ft_putstr(info->arg->name);
		if (--nb > 0)
			ft_putchar(' ');
	}
	info->arg = info->arg->next;
	while (info->arg != tmp)
	{
		if (info->arg->name != NULL && info->arg->selected == 1)
		{
			ft_putstr(info->arg->name);
			if (--nb > 0)
				ft_putchar(' ');
		}
		info->arg = info->arg->next;
	}
	info->arg = tmp;
}

void				selecte(t_info *info)
{
	t_arg			*tmp;

	tmp = info->arg;
	while (tmp->current != 1)
		tmp = tmp->next;
	if (tmp->selected == 0)
		tmp->selected = 1;
	else
		tmp->selected = 0;
	right(info);
}

void				free_fake_node(t_info *info)
{
	t_arg			*start;

	start = info->arg;
	info->arg = info->arg->prev;
	while (info->arg->name == NULL)
	{
		info->arg->next->prev = info->arg->prev;
		info->arg->prev->next = info->arg->next;
		free(info->arg);
		info->arg = start;
		info->arg = info->arg->prev;
	}
	info->arg = start;
	screen_clear();
}

int					delet(t_info *info)
{
	t_arg			*begin;
	int				nb;

	if (info->arg == info->arg->next)
	{
		free_node(info->arg);
		return (1);
	}
	begin = info->arg;
	free_fake_node(info);
	while (info->arg->current != 1)
		info->arg = info->arg->next;
	info->arg->prev->next = info->arg->next;
	info->arg->next->prev = info->arg->prev;
	info->arg->next->current = 1;
	if (info->arg->start == 1)
		begin = info->arg->next;
	if (info->arg->next->start != 1)
		info->arg->next->start = info->arg->start;
	free_node(info->arg);
	info->arg = begin;
	fill_info_size(info, info->nb_param);
	nb = (info->column * info->line) - info->nb_param;
	fake_node(info->arg->prev, nb, info->arg);
	return (0);
}
