/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_term.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/18 12:56:08 by rvievill          #+#    #+#             */
/*   Updated: 2016/09/14 12:30:07 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_select.h"

void					init_termios(struct termios *info)
{
	info->c_lflag &= ~(ICANON | ECHO);
	info->c_cc[VMIN] = 1;
	info->c_cc[VTIME] = 0;
	tputs(tgetstr("vi", 0), 1, my_putchar);
}

int						init_term(int ac, t_info *info, char **av)
{
	char				*name;
	struct termios		term_cur;

	if ((name = getenv("TERM")) == NULL)
		return (put_error(-1));
	if ((tgetent(NULL, name)) == ERR)
		return (put_error(3));
	if (tcgetattr(0, &(info->term)) == -1)
		return (put_error(4));
	name = ttyname(0);
	if ((info->fd = open(name, O_WRONLY)) == -1)
		return (-1);
	term_cur = info->term;
	init_termios(&term_cur);
	if ((tcsetattr(0, TCSADRAIN, &(term_cur))) == -1)
		return (put_error(5));
	create_lst(ac, ++av, info);
	screen_clear();
	return (0);
}

int						term_default(t_info *info)
{
	close(info->fd);
	screen_clear();
	if (tcgetattr(0, &(info->term)) == -1)
		return (put_error(4));
	info->term.c_lflag |= (ICANON | ECHO);
	if (tcsetattr(0, 0, &info->term) == -1)
		return (put_error(5));
	tputs(tgetstr("ve", 0), 1, my_putchar);
	if (info->enter == 1)
		enter(info);
	return (0);
}
